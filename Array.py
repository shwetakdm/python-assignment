#sum of array
'''arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
a = sum(arr)
print("sum of the array is ",a)'''

#array
'''list = [1,2,3,4,5,6,7,8,9,10]
list.pop(2)
print(list)'''

#sort descending
'''list = [1,7,5,4,2,6,8,10,9,3]
list.sort(reverse=True)
print(list)'''

#sort ascending
'''list = [1,7,5,4,2,6,8,10,9,3]
list.sort()
print(list)'''

#sum_mean_max_min
'''list =[1,10,20,35,40,56,67,33,89,100]
sum=sum(list)
max=max(list)
min=min(list)
mean=sum/10
print("greatest number is :",list[-1])
print("sum of number is :",sum)
print("maximum number is :",max)
print("minimum number is :",min)
print("mean is :",mean)'''


